PJE2: analyse de sentiments avec twitter.
Antoine Amara, Maiz Nabil.

Pour fonctionner cette application a besoin des outils suivants:
- NodeJS et son gestionnaire de paquet npm.
- L'outil make pour compiler la partie java.

Pour installer l'application, placez vous a la racine du projet et entrez la commande suivante:
- ./install

bower va vous demandez quelle version de angularjs il doit installer, selectionner la numero 3 et valider.

Enfin pour demarrer l'application, entrez la commande:

node main.js

Pour visualiser l'interface graphique, rendez-vous à l'addresse suivante via votre navigateur web:
http://localhost:9000

On notera que pour tester la prediction, un ensemble d'apprentissage est present a la racine du projet(train.csv), c'est sur cette base
que nous avons effectuer notre analyse des algorithmes de prediction.
