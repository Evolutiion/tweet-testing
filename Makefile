all:
	javac app/javasrc/pje/twitter/*.java
	javac app/javasrc/pje/datamining/*.java
	javac app/javasrc/pje/parser/*.java

clean:
	rm app/javasrc/pje/twitter/*.class
	rm app/javasrc/pje/datamining/*.class
	rm app/javasrc/pje/parser/*.class
