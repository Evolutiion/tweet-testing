'use strict';

angular.module('TwitterGui')
.directive('twitterSearch', () => {
  return {
    scope: {
      placeholder: '=',
      search: '&'
    },
    templateUrl: 'app/angularjs/directives/twitter-search/twitter-search.html'
  };
});
