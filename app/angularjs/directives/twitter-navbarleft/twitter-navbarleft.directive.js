'use strict'

angular.module('TwitterGui')
.directive('twitterNavbarleft', () => {
  return {
    templateUrl: 'app/angularjs/directives/twitter-navbarleft/twitter-navbarleft.html',
    transclude: true
  };
});
