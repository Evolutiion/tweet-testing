'use strict';

angular.module('TwitterGui')
.directive('twitterGui', ['MultiTransclude', (MultiTransclude) => {
  return {
    transclude: true,
    scope: {},
    templateUrl: 'app/angularjs/directives/twitter-gui/twitter-gui.html',
    link: (scope, element, attrs, ctrl, transclude) => {
      MultiTransclude.transclude(element, transclude);
    }
  };
}]);
