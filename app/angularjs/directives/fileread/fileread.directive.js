angular.module('TwitterGui')
.directive('fileread', () => {
  return {
    scope: {
      fileread: '=',
      fnt: '&'
    },
    link: (scope, element, attrs) => {
      element.bind('change', function(changeEvent) {
        scope.$apply(function() {
	  console.log('file:',scope.fileread = changeEvent.target.files[0]);
          scope.fileread = changeEvent.target.files[0].name;
          scope.fnt({file: scope.fileread});
        });
      });
    }
  }
})
