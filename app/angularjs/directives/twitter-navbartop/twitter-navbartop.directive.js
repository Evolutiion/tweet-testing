'use strict';

angular.module('TwitterGui')
.directive('twitterNavbartop', () => {
  return {
    templateUrl:'app/angularjs/directives/twitter-navbartop/twitter-navbartop.html'
  };
});
