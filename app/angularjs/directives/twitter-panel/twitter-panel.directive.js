'use strict';

angular.module('TwitterGui')
.directive('twitterPanel', ['MultiTransclude', (MultiTransclude) => {
  return {
    scope: {
      size: '='
    },
    templateUrl: 'app/angularjs/directives/twitter-panel/twitter-panel.html',
    transclude: true,
    link: (scope, element, attrs, ctrl, transclude) => {
      MultiTransclude.transclude(element, transclude);
    }
  };
}]);
