'use strict';

angular.module('TwitterGui')
.directive('twitterPiechart', () => {
  return {
    scope: {
      datas: '='
    },
    templateUrl: 'app/angularjs/directives/twitter-piechart/twitter-piechart.html',
    link: (scope, element, attrs) => {
      var chart = null;

      var options = {
        series: {
          pie: {
            show: true,
            radius: 1,
            label: {
              show: true,
              radius: 2/3,
              formatter: labelFormatter,
              threshold: 0.1
            }
          }
        }
      };

      function labelFormatter(label, series) {
       return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
     };

      scope.$watch('datas', (newValue, oldValue) => {
        console.log('data changes', newValue);
        if(!chart) {
          chart = $.plot(element, newValue, options);
          element.show();
        }
        else {
          chart.setData(newValue);
          chart.setupGrid();
          chart.draw();
        }
      });
    }
  }
})
