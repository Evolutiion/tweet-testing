'use strict';

angular.module('TwitterGui')
.directive('twitterList', () => {
  return {
    scope: {
      items: '=',
      selected: '=',
      class: '=',
      id: '='
    },
    templateUrl: 'app/angularjs/directives/twitter-list/twitter-list.html',
    link: (scope, element, attrs) => {

      scope.selectItem = (item) => {
        scope.selected = item;
      };

    }
  };
});
