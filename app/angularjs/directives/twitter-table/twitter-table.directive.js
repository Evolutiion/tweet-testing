'use strict';

angular.module('TwitterGui')
.directive('twitterTable', () => {
  return {
    scope: {
      head: '=',
      datas: '=',
      classes: '=',
      currentclass: '='
    },
    templateUrl: 'app/angularjs/directives/twitter-table/twitter-table.html',
    controller: ($scope) => {
      if($scope.head !== undefined) {
        $scope.heading = true;
      }

      this.updateTweet = (id, item) => {
        $scope.datas[id].classe = item;
      };
    }
  };
});
