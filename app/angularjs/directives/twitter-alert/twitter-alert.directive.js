angular.module('TwitterGui')
.directive('twitterAlert', () => {
  return {
    transclude: true,
    templateUrl: 'app/angularjs/directives/twitter-alert/twitter-alert.html',
    scope: {
      classe: '='
    }
  };
});
