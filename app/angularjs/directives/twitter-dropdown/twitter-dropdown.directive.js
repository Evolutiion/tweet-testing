angular.module('TwitterGui')
.directive('twitterDropdown', () => {
  return {
    require: '^twitterTable',
    scope: {
      tweetid: '=',
      selected: '=',
      items: '=',
      sized: '@'
    },
    transclude: true,
    replace: true,
    templateUrl: 'app/angularjs/directives/twitter-dropdown/twitter-dropdown.html',
    controller: ($scope) => {
      $scope.status = {
        isopen: false
      };

      $scope.toggleDropdown = ($event) => {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.status.isopen = !$scope.status.isopen;
      };

      $scope.select = (item) => {
        $scope.selected = item;
        this.updateTweet($scope.tweetid, item);
      };
    }
  };
});
