'use strict';

angular.module('TwitterGui', ['ngRoute', 'ui.bootstrap'])
.config(['$routeProvider', ($routeProvider) => {
  $routeProvider
  .when('/search', {
    templateUrl: 'app/angularjs/partials/search.html',
    controller: 'SearchCtrl'
  })
  .when('/predict', {
    templateUrl: 'app/angularjs/partials/predict.html',
    controller: 'PredictCtrl'
  })
  .otherwise({
    redirectTo: '/search'
  });
}]);
