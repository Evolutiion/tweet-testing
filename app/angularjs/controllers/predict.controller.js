'use strict'

angular.module('TwitterGui')
.controller('PredictCtrl', ['$scope', 'datamining', 'twitter', ($scope, datamining, twitter) => {
  $scope.navbarleftItems = [
    {href: '#/search', name: 'Search in Twitter', icon: 'fa fa-search fa-fw'},
    {href: '#/predict', name: 'Predict', icon: 'fa fa-twitter-square fa-fw'},
  ];
  $scope.navbarleftCurrent = 'Predict';

  $scope.algo = {name: 'KNN'};

  $scope.config = {tweets: undefined};

  $scope.d = [
    {
      label: 'negatives',
      data: 50,
      color: '#cb4b4b'
    },
    {
      label: 'neutrals',
      data: 30,
      color: '#afd8f8'
    },
    {
      label: 'positive',
      data: 20,
      color: '#4da74d'
    }
  ];

  $scope.selectAlgo = (name) => {
    $scope.algo.name = name;
  };

  $scope.configKNN = (config) => {
    datamining.initKNN(config, (err, status) => {
      $scope.modelKNNOk = true;
      $scope.$apply();
    });
  };

  $scope.configBayes = (config) => {
    console.log('configs:',config);
    datamining.initBayes(config, (err, status) => {
      $scope.modelBayesOK = true;
      $scope.$apply();
    });
  };

  $scope.predict = (configs) => {
    datamining.predictKNN(configs.tweet, configs.k, (err, prediction) => {
      if(err) console.log('failed to predict');

      $scope.prediction = prediction;
      $scope.$apply();
    });
  };

  $scope.predictBatchKNN = (configs) => {
    console.log('predict batch KNN service');
    twitter.save('rawdata', false, configs.tweets, (err, status) =>{
      configs.file = 'clean.csv';
      datamining.predictKNNBatch(configs, (err, datas) =>{
        if(err) console.log('cannot predict in batch mode');

        $scope.datas = datas;
        $scope.$apply();
      });
    });
  };

  $scope.predictBayes = (configs) => {
    console.log('config state in ctrl:', configs);
    datamining.predictBayes(configs.tweet, (err, prediction) => {
      if(err) console.log('failed to predict');

      $scope.prediction = prediction;
      $scope.$apply();
    });
  };

  $scope.predictBatchBayes = (configs) => {
    console.log('predict batch KNN service');
    twitter.save('rawdata', false, configs.tweets, (err, status) =>{
      console.log('save OK');
      configs.file = 'clean.csv';
      datamining.predictBayesBatch(configs, (err, datas) =>{
        console.log('predict bayes OK');
        if(err) console.log('cannot predict in batch mode');

        $scope.datas = datas;
        $scope.$apply();
      });
    });
  };

  $scope.go = (query) => {
    console.log('query');
    twitter.query(query, (err, ts) => {
      $scope.config.tweets = ts;
      $scope.$apply();
    });
  };

}]);
