'use strict'

angular.module('TwitterGui')
.controller('SearchCtrl', ['$scope', 'twitter', ($scope, twitter) => {
  $scope.navbarleftItems = [
    {href: '#/search', name: 'Search in Twitter', icon: 'fa fa-search fa-fw'},
    {href: '#/predict', name: 'Predict', icon: 'fa fa-twitter-square fa-fw'},
  ];
  $scope.navbarleftCurrent = 'Search in Twitter';

  $scope.tablehead = ['tweet', 'classe'];

  $scope.classes = ['1', '0', '-1'];
  $scope.currentclass = '0';

  $scope.go = (query) => {
    twitter.query(query, (err, ts) => {
      $scope.tweets = ts;
      $scope.$apply();
    });
  };

  $scope.save = () => {
    twitter.save('rawdata', true, $scope.tweets, (err, status) => {
    });
  };
}]);
