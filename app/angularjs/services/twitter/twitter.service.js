'use strict'

angular.module('TwitterGui')
.factory('twitter', () => {
  return {
    query: (query, callback) => {
      var socket = io.connect('http://localhost:9000/');
      socket.emit('getTweets', query);
      socket.on('returnTweets', (tweets) => {
        callback('', tweets);
      });
    },
    save: (filename, destructclean, tweets, callback) => {
      var classes = [];
      var socket = io.connect('http://localhost:9000/');
      angular.forEach(tweets, (value, key) => {
        classes.push(value.classe);
      });
      socket.emit('saveTweets', filename, destructclean, classes);
      socket.on('returnSave', (status) => {
        callback('', status);
      });
    }
  };
});
