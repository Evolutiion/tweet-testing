'use strict'

angular.module('TwitterGui')
.factory('datamining', () => {
  return {
    initKNN: (configs, callback) => {
      var socket = io.connect('http://localhost:9000/');
      socket.emit('initKNN', configs);
      socket.on('initStatus', (status) => {
        callback('', status);
      });
    },
    predictKNN: (tweet, k, callback) => {
      var socket = io.connect('http://localhost:9000/');
      socket.emit('predictKNN', tweet, k);
      socket.on('predictionKNN', (err, prediction) => {
        if(err) console.log('cannot predict');

        callback('', prediction);
      });
    },
    predictKNNBatch: (configs, callback) => {
      console.log('configs in service:', configs);
      var socket = io.connect('http://localhost:9000/');
      socket.emit('predictKNNBatch', configs);
      socket.on('KNNBatchResults', (err, datas) =>{
        console.log('err', err);
        if(err) console.log('cannot predict in batch mode');

        console.log('datas:', datas);
        callback('', datas);
      });
    },
    initBayes: (configs, callback) => {
      var socket = io.connect('http://localhost:9000/');
      socket.emit('initBayes', configs);
      socket.on('initBayesStatus', (status) => {
        callback('', status);
      });
    },
    predictBayes: (tweet, callbak) => {
      var socket = io.connect('http://localhost:9000/');
      socket.emit('predictBayes', tweet);
      socket.on('bayesPrediction', (err, prediction) => {
        callbak('', prediction);
      });
    },
    predictBayesBatch: (configs, callback) => {
      console.log('configs in service:', configs);
      var socket = io.connect('http://localhost:9000/');
      socket.emit('predictBayesBatch', configs);
      socket.on('BayesBatchResults', (err, datas) =>{
        console.log('err', err);
        if(err) console.log('cannot predict in batch mode');

        console.log('datas:', datas);
        callback('', datas);
      });
    }
  };
});
