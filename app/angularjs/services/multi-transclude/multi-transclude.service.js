'use strict';

angular.module('TwitterGui')
.factory('MultiTransclude', () => {
  return {
    transclude: (element, transcludeFn) => {
      transcludeFn((clone) => {
        angular.forEach(clone, (cloneEl) => {
          if(cloneEl.attributes != undefined) {
            // get desired target ID.
            var tId = cloneEl.attributes["transclude-to"].value;

            // find target element with that ID.
            var target = element.find(`[transclude-id="${tId}"]`);

            // append element to target.
            if(target.length) {
              target.append(cloneEl);
            }
            else {
              cloneEl.remove();
              throw new Error(
                `Target not found. Please specify the correct transclude-to attribute`
              );
            }
          }
        });
      });
    }
  };
});
