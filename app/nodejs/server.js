'use strict';

var twitter = require('./modules/twitter');
var datamining = require('./modules/datamining');
var app = require('http').createServer(handler);
var io = require('socket.io').listen(app);
var fs = require('fs');
var url = require('url');
var path = require('path');

function handler (request, response) {
  var uri = url.parse(request.url).pathname
  , filename = path.join(process.cwd(), uri);

  fs.exists(filename, function(exists) {
    if(!exists) {
      response.writeHead(404, {"Content-Type": "text/plain"});
      response.write("404 Not Found\n");
      response.end();
      return;
    }

    if (fs.statSync(filename).isDirectory()) filename += 'index.html';

    fs.readFile(filename, "binary", function(err, file) {
      if(err) {
        response.writeHead(500, {"Content-Type": "text/plain"});
        response.write(err + "\n");
        response.end();
        return;
      }

      response.writeHead(200);
      response.write(file, "binary");
      response.end();
    });
  });
}

module.exports = {
  connect: function() {

    app.listen(9000);

    io.sockets.on('connection', function (socket) {
      socket.on('getTweets', function (query) {
        twitter.getTweets(query, function(err, tweets) {
          if(err) console.log('Error cannot get tweets');
          socket.emit('returnTweets', tweets);
        });
      });

      socket.on('saveTweets', function(filename, destruct, classes) {
        twitter.saveTweets(filename, destruct, classes, function(err, status) {
          if(err) console.log('Error cannot save tweets');
          socket.emit('returnSave', status);
        });
      });

      socket.on('initKNN', function(configs) {
        datamining.initKNN(configs, function(err, status) {
          if(err) console.log('Error cannot init KNN algorithm');

          socket.emit('initStatus', status);
        });
      });

      socket.on('predictKNN', function(tweet, k) {
        datamining.predictKNN(tweet, k, function(err, predict) {
          if(err) console.log('Error cannot predict');

          socket.emit('predictionKNN', err, predict);
        });
      });

      socket.on('predictKNNBatch', function(configs) {
        datamining.predictKNNBatch(configs, function(err, datas) {
          if(err) console.log('error, cannot predict');

          socket.emit('KNNBatchResults', err, datas);
        });
      });

      socket.on('initBayes', function(configs) {
        datamining.initBayes(configs, function(err, status) {
          if(err) console.log('Error cannot init Bayes');

          socket.emit('initBayesStatus', status);
        });
      });

      socket.on('predictBayes', function(tweet) {
        datamining.predictBayes(tweet, function(err, prediction) {
          if(err) console.log('Error cannot predict with Bayes');

          socket.emit('bayesPrediction', err, prediction)
        });
      });

      socket.on('predictBayesBatch', function(configs) {
        datamining.predictBayesBatch(configs, function(err, datas) {
          if(err) console.log('error, cannot predict');

          console.log('datas in server node:',datas);
          socket.emit('BayesBatchResults', err, datas);
        });
      });

    });

  }
}
