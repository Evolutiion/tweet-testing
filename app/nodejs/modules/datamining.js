'use strict'

var java = require('java');
var fs = require('fs');

java.classpath.push('./app/javasrc');

var KNN = java.import('pje.datamining.KNN');
var NaiveBayes = java.import('pje.datamining.NaiveBayes');
var knn;
var bayes

module.exports = {
  initKNN: function(configs, callback) {
    console.log('init KNN in nodejs');
    console.log(configs.file);
    knn = new KNN(configs.file, parseInt(configs.N));

    callback('', 'OK');
  },
  predictKNN: function(tweet, k, callback) {
    console.log('predict KNN in nodejs');
    java.callMethod(knn, 'predict', tweet, parseInt(k), function(err, prediction) {
      if(err) console.log('cannot predict with KNN', err);

      console.log('prediction:', prediction);

      callback(err, prediction);
    });
  },
  predictKNNBatch: function(configs, callback) {
    console.log('predict KNN Batch in nodejs');
    console.log('configs', configs);
    java.callMethod(knn, 'predictBatch', configs.file, parseInt(configs.k), function(err, datas) {
      console.log(datas);
      if(err) console.log('cannot predict KNN in Batch mode');

      var jsondata = [];

      jsondata.push(JSON.parse(datas[0]));
      jsondata.push(JSON.parse(datas[1]));
      jsondata.push(JSON.parse(datas[2]));



      callback(err, jsondata);
    });
  },
  initBayes: function(configs, callback) {
    console.log('file',configs.file);
    console.log('init Naive Bayes in nodejs');
    bayes = new NaiveBayes(configs.file);

    callback('','OK');
  },
  predictBayes: function(tweet, callback) {
    console.log('predict Bayes in nodejs');
    java.callMethod(bayes, 'OrganizeProba', false, function(err) {
      console.log(err);
      if(err) console.log("cannot organizeProba")
      java.callMethod(bayes, 'predict', tweet, function(err, prediction) {
        if(err) console.log('cannot predict with Bayes');

        console.log('prediction', prediction);

        callback(err, prediction);
      });
    });
  },
  predictBayesBatch: function(configs, callback) {
    console.log('predict Bayes Batch in nodejs');
    console.log('configs', configs);
    java.callMethod(bayes, 'OrganizeProba', false, function(err) {
      console.log(err);
      if(err) console.log("error cannot organize proba");
      java.callMethod(bayes, 'predictBatch', configs.file, function(err, datas) {
        console.log(datas);
        if(err) console.log('cannot predict KNN in Batch mode');

        var jsondata = [];

        jsondata.push(JSON.parse(datas[0]));
        jsondata.push(JSON.parse(datas[1]));
        jsondata.push(JSON.parse(datas[2]));

        callback(err, jsondata);
      });
    });
  }
}
