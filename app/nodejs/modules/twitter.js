'use strict'

var java = require('java');

java.classpath.push('/home/antoine/electron/app/javasrc');

java.classpath.push('./app/javasrc/lib/twitter4j-async-4.0.4.jar');
java.classpath.push('./app/javasrc/lib/twitter4j-core-4.0.4.jar');
java.classpath.push('./app/javasrc/lib/twitter4j-media-support-4.0.4.jar');
java.classpath.push('./app/javasrc/lib/twitter4j-stream-4.0.4.jar');
java.classpath.push('./app/javasrc/lib/json_simple-1.1.jar');

var TwitterConfig = java.import('pje.twitter.TwitterConfig');
var TwitterData = java.import('pje.twitter.TwitterData');
var Proxy = java.import('pje.twitter.Proxy');

var proxy = new Proxy("cache-etu.univ-lille1.fr", 3128, "antoine", "pass");
var configs = new TwitterConfig("21lTVdWnDT7VrQBxCFuhXvC1B",
"jcURvd292a2WucsPxKKOGluxP4uLXdygvODMAkcXZfvikSxGkj",
"3736383317-E7GnTlalDf15X7LgVuAfvwIlvsjpSJD4zVIsMB2",
"hfHpgVQMly02SwhgwtbgFxbwlVIAXcGEbNPWdFBdoAxYE");
var tweet = new TwitterData(configs);


module.exports = {
  getTweets : function(query, callback) {
    java.callMethod(tweet, 'query', query, function(err, results) {
      if(err) console.log('Error, can\'t call query');

      callback(err, JSON.parse(results.toString()));
    });
  },
  saveTweets : function(filename, destructclean, classes, callback) {
    java.callMethod(tweet, 'export', filename, classes, function(err, results) {
      if(err) console.log('Error, cannot save datas');

      //clean data.
      var DataCleaner = java.import('pje.datamining.DataCleaner');
      var cleaner = new DataCleaner(filename+'.csv');
      cleaner.clean(destructclean);

      callback(err, 'saved');
    });
  }
}
