package pje.twitter;

/**
* Classe représentant un serveur proxy, elle permet donc de sauvegarder les paramètres de celui-ci.
*
* @author Maiz Nabil, Amara Antoine
* @version 1.0
*/

public class Proxy {

  private String host;
  private String user;
  private String pass;
  private int port;

  /**
  * Constructeur de base, permettant de se connecter anonymement avec un minimum d'information
  * c'est-à-dire que seul l'adresse du proxy est demandée. On notera que le port sera par défaut 3128.
  * @param host
  *           L'adresse du proxy, elle est sous la forme d'une url (e.g http://proxy.com) mais peut aussi être
  *           une adresse ip (e.g http://192.168.1.26).
  */
  public Proxy(String host) {
    this.host = host;
    this.port = 3128;
  }

  /**
  * Constructeur permettant de se connecter anonymement, l'adresse ainsi que le port du proxy
  * doivent être précisés.
  * @param host
  *           L'adresse du proxy, elle est sous la forme d'une url (e.g http://proxy.com) mais peut aussi être
  *           une adresse ip (e.g http://192.168.1.26).
  * @param port
  *           Le port d'écoute du proxy.
  */
  public Proxy(String host, int port) {
    this.host = host;
    this.port = port;
  }

  /**
  * Constructeur permettant de se connecter via un utilisateur, l'adresse, le port,
  * un nom d'utilisateur et un mot de passe doivent être précisés.
  * @param host
  *           L'adresse du proxy, elle est sous la forme d'une url (e.g http://proxy.com) mais peut aussi être
  *           une adresse ip (e.g http://192.168.1.26).
  * @param port
  *           Le port d'écoute du proxy.
  * @param user
  *           Le nom d'utilisateur dans le cas ou le proxy demande une authentification.
  * @param pass
  *           Le mot de passe correspondant à l'utilisateur.
  */
  public Proxy(String host, int port, String user, String pass) {
    this.host = host;
    this.port = port;
    this.user = user;
    this.pass = pass;
  }

  /**
  * Setter permettant de mettre à jour l'adresse du proxy.
  * @param host
  *           L'adresse du proxy, elle est sous la forme d'une url (e.g http://proxy.com) mais peut aussi être
  *           une adresse ip (e.g http://192.168.1.26).
  *
  */
  public void setHost(String host) {
    this.host = host;
  }

  /**
  * Getter permettant de récupérer l'adresse du proxy.
  */
  public String getHost() {
    return host;
  }

  /**
  * Setter permettant de mettre à jour le port d'écoute du proxy.
  * @param port
  *           Le port d'écoute du proxy.
  */
  public void setPort(int port) {
    this.port = port;
  }

  /**
  * Getter permettant de récupérer le port d'écoute du proxy.
  */
  public int getPort() {
    return port;
  }

  /**
  * Méthode permettant de savoir si une authentification par utilisateur/mot de passe
  * a été configurée.
  */
  public boolean haveUserAuth() {
    if(user != null && pass != null)
      return true;

    return false;
  }

  /**
  * Setter permettant de mettre à jour les informations d'authentification.
  * @param user
  *           Le nom d'utilisateur utilisé pour l'authentification.
  * @param pass
  *           Le mot de passe correspondant à l'utilisateur.
  */
  public void setAuthInfos(String user, String pass) {
    this.user = user;
    this.pass = pass;
  }

  /**
  * Getter permettant de récupérer l'utilisateur pour l'authentification.
  */
  public String getUser() {
    return user;
  }

  /**
  * Getter permettant de récupérer le mot de passe pour l'authentification.
  */
  public String getPass() {
    return pass;
  }
}
