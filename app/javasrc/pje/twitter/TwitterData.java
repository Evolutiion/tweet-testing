package pje.twitter;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import java.lang.Long;
import pje.parser.CSVWriter;
import org.json.simple.JSONObject;

/**
* Classe permettant d'effectuer la connexion vers la base de données de twitter puis de récupérer des tweets
* via des requêtes et de les mettres en forme pour l'affichage.
*
* @author Maiz Nabil, Amara Antoine
* @version 1.0
*/

public class TwitterData {

  private TwitterConfig config;
  private Twitter twitter;
  private QueryResult result;

  /**
  * Constructeur permettant d'initialiser la connexion avec la base de données de twitter.
  * @param config
  *             Un objet contenant les informations de connexions permettant d'établir celle-ci.
  */
  public TwitterData(TwitterConfig config) {
    this.config = config;
    this.twitter = config.connect();
    this.result = null;
  }

  /**
  * Méthode permettant d'effectuer une requête sur la base de données. Celle-ci nous renverra les tweets
  * correspondant à notre requête.
  * @param query
  *           La requête qui sera transmise à la base de données. Pour savoir comment la construire, il faut
  *           consulter la documentation de l'api twitter4j: http://twitter4j.org/oldjavadocs/4.0.5-SNAPSHOT/index.html
  * @return
  *       Cette fonction ne renvoie rien, les résultats de la requête sont stockées dans l'objet.
  *       Pour obtenir les résultats, il faut appelé la méthode toString de l'objet,
  *       celle-ci mettra en forme le résultat.
  */
  public JSONObject query(String query) {
    try {
      Query q = new Query(query);
      q.lang("fr");
      q.count(50);
      result = twitter.search(q);
    }catch(TwitterException e) {
      e.printStackTrace();
      System.out.println("Failed to reach tweet " + e.getMessage());
    }
    return toJSON();
  }

  private JSONObject toJSON() {
    JSONObject tweets = new JSONObject();
    int i = 0;

    for(Status status : result.getTweets()) {
      JSONObject obj = new JSONObject();
      obj.put("tweet", "@"+status.getUser().getScreenName() + ":"+status.getText());
      obj.put("classe", "0");
      tweets.put(i, obj);
      i++;
    }
    return tweets;
  }

  /**
  * Cette méthode permet de mettre à jour les configurations de connexion de la base de données
  * et de relancer la connexion avec celle-ci.
  * @param config
  *             L'objet contenant les nouvelles informations de connexions.
  */
  public void reconnect(TwitterConfig config) {
    this.config = config;
    this.config.connect();
  }

  public void export(String filename) {
    CSVWriter writer = new CSVWriter(filename+".csv");
    if(result != null) {
      for(Status status : result.getTweets()) {
        String[] line = new String[5];
        line[0] = new Long(status.getId()).toString();
        line[1] = status.getUser().getScreenName();
        line[2] = status.getText().replaceAll("\\n", " ");
        line[3] = status.getCreatedAt().toString();
        line[4] = new String("-1");

        writer.writeLine(line);
      }
    }
    else {
      System.out.println("No tweet to export");
      return;
    }
  }

  public void export(String filename, String[] classes) {
    int i = 0;
    CSVWriter writer = new CSVWriter(filename+".csv");
    if(result != null) {
      for(Status status : result.getTweets()) {
        String[] line = new String[5];
        line[0] = new Long(status.getId()).toString();
        line[1] = status.getUser().getScreenName();
        line[2] = status.getText().replaceAll("\\n", " ");
        line[3] = status.getCreatedAt().toString();
        line[4] = classes[i];

        writer.writeLine(line);
        i++;
      }
    }
    else {
      System.out.println("No tweet to export");
      return;
    }
  }

  /**
  * Méthode permettant de mettre en forme les résultats d'une requête et de les renvoyer sous la forme
  * d'une chaine de caractère.
  */
  public String toString() {
    String s = new String("Results: \n");
    if(result != null) {
      for(Status status : result.getTweets()) {
        s += "@"+status.getUser().getScreenName() + ":"+status.getText()+"\n";
      }
    }
    else
    s = "No result";

    return s;
  }
}
