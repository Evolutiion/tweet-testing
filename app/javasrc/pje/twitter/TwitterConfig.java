package pje.twitter;

import twitter4j.conf.ConfigurationBuilder;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedWriter;
import java.util.Properties;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
* Classe permettant de sauvegarder les informations d'authentifications permettant l'accès à la base de données
* de twitter. Elle permet également d'établir la connexion avec la base.
*
* @author Maiz Nabil, Amara Antoine
* @version 1.0
*/

public class TwitterConfig {

  private String consumerKey;
  private String consumerSecret;
  private String accessToken;
  private String accesTokenSecret;
  private Proxy proxy;
  private ConfigurationBuilder cb;

  /**
  * Constructeur par défaut permettant d'initialiser les configurations à partir du fichier twitter4j.properties.
  */
  public TwitterConfig() {
    File configs = new File("twitter4j.properties");
    FileInputStream is;
    FileOutputStream os;
    Properties prop = new Properties();

    try {
      if(configs.exists()) {
        is = new FileInputStream(configs);
        prop.load(is);
        consumerKey = prop.getProperty("oauth.consumerKey");
        consumerSecret = prop.getProperty("oauth.consumerSecret");
        accessToken = prop.getProperty("oauth.accessToken");
        accesTokenSecret = prop.getProperty("oauth.accessTokenSecret");

        if(prop.getProperty("http.proxyHost").equals("") && !prop.getProperty("http.proxyPort").equals("")) {
          proxy = new Proxy(prop.getProperty("http.proxyHost"), new Integer(prop.getProperty("http.proxyPort")));

          if(!prop.getProperty("http.proxyUser").equals("") && !prop.getProperty("http.proxyPassword").equals("")) {
            proxy.setAuthInfos(prop.getProperty("http.proxyUser"), prop.getProperty("http.proxyPassword"));
          }
        }
        else {
          proxy = null;
        }
      }
    }catch(IOException e) {
      System.out.println("IO Error !!");
      e.printStackTrace();
    }
    cb = new ConfigurationBuilder();
  }

  /**
  * Constructeur permettant de configurer la connexion vers la base de données de twitter sans proxy.
  * Les paramètres consumerKey, consumerSecret, accessToken et accesTokenSecret sont les paramètres
  * permettant la connexion à la base de données de twitter. Ces informations sont données après la création
  * d'une application via le site: https://apps.twitter.com/
  */
  public TwitterConfig(String consumerKey, String consumerSecret, String accessToken, String accesTokenSecret) {
    this.consumerKey = consumerKey;
    this.consumerSecret = consumerSecret;
    this.accessToken = accessToken;
    this.accesTokenSecret = accesTokenSecret;
    this.proxy = null;
    cb = new ConfigurationBuilder();
    writeConfig();
  }

  /**
  * Constructeur permettant de configurer la connexion vers la base de données de twitter via un proxy.
  * Les paramètres consumerKey, consumerSecret, accessToken et accesTokenSecret sont les paramètres
  * permettant la connexion à la base de données de twitter. Ces informations sont données après la création
  * d'une application via le site: https://apps.twitter.com/
  * @param proxy
  *             Un objet qui contient les configurations du proxy.
  */
  public TwitterConfig(String consumerKey, String consumerSecret, String accessToken, String accesTokenSecret, Proxy proxy) {
    this.consumerKey = consumerKey;
    this.consumerSecret = consumerSecret;
    this.accessToken = accessToken;
    this.accesTokenSecret = accesTokenSecret;
    this.proxy = proxy;
    cb = new ConfigurationBuilder();
    writeConfig();
  }

  /**
  * Méthode permettant d'établir la connexion avec la base de données de twitter en fonction
  * des paramètres précedemment configurés.
  * @return
  *       Un objet Twitter, provenant de l'api, cet objet permettra d'effectuer des requêtes
  *       sur la base de données.
  */
  public Twitter connect() {
    // on ajoute les configurations de base.
    cb.setDebugEnabled(true)
    .setOAuthConsumerKey(consumerKey)
    .setOAuthConsumerSecret(consumerSecret)
    .setOAuthAccessToken(accessToken)
    .setOAuthAccessTokenSecret(accesTokenSecret);

    // si on a choisit une configuration via proxy, on configure celle-ci.
    if(proxy != null)
    connectProxy();

    // connexion.
    return new TwitterFactory(cb.build()).getInstance();
  }

  private void writeConfig() {
    File configs = new File("twitter4j.properties");

    try {
      FileWriter writer = new FileWriter(configs, true);
      BufferedWriter output = new BufferedWriter(writer);
      output.write("oauth.consumerKey="+consumerKey+
      "\noauth.consumerSecret="+consumerSecret+
      "\noauth.accessToken="+accessToken+
      "\noauth.accessTokenSecret="+accesTokenSecret);

      if(proxy != null) {

        if(!proxy.getHost().equals("") && proxy.getPort() != 0) {
          output.write("\nhttp.proxyHost="+proxy.getHost()+
          "\nhttp.proxyPort="+proxy.getPort());
        }

        if(!proxy.getUser().equals("") && !proxy.getPass().equals("")) {
          output.write("\nhttp.proxyUser="+proxy.getUser()+
          "\nhttp.proxyPassword="+proxy.getPass());
        }
      }
      output.flush();
      output.close();
    }catch(IOException e) {
      System.out.println("IOError");
      e.printStackTrace();
    }
  }

  /**
  * Méthode permettant d'ajouter la configuration du proxy aux paramètres de connexion de la base
  * de données de twitter.
  */
  private void connectProxy() {
    // ajout des paramètres supplémentaire.
    cb.setHttpProxyHost(proxy.getHost())
    .setHttpProxyPort(proxy.getPort());

    // si on a configuré une connexion via une authentification utilisateur/mot de passe, on les ajoute.
    if(proxy.haveUserAuth()) {
      cb.setHttpProxyUser(proxy.getUser())
      .setHttpProxyPassword(proxy.getPass());
    }
  }

}
