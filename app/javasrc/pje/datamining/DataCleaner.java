package pje.datamining;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;


public class DataCleaner implements Cleaner{

	private String toRead;

	public DataCleaner(String toRead){
		this.toRead = toRead;
	}

	public void clean(boolean destruct){

		String line;
		String[] res = new String[5];

		FileReader fr;
		try {
			fr = new FileReader(this.toRead);
			BufferedReader br = new BufferedReader(fr);

			FileOutputStream fos = new FileOutputStream("clean.csv", destruct);

			while((line = br.readLine()) != null) {
				res = line.split(";");
				String str = (res[2].replaceAll("\\s?[@#][a-zA-Z0-9-_\\.]+\\s?:?\\s?", "").replaceAll(":?\\s?https?:\\/\\/[a-zA-Z0-9\\._-…]+\\/?[a-zA-Z0-9,\\.,_,-…]*\\s?", "").replaceAll(":?RT\\s?", "").replaceAll("^\\w ÀÂÆÇÉÈÊËÎÏÔŒÙÛÜŸàtâtætçtétètêtëtîtïtôtœtùtûtütÿ","").replaceAll("\\s?[!.?…]", "").replaceAll("'", " "));
				fos.write((res[0] + ";" + res[1] + ";" + str + ";" + res[3] + ";" + res[4] + "\n").getBytes());
			}

			br.close();
			fos.close();

		} catch (Exception e1) {
			System.out.println("File not found !");
			e1.printStackTrace();
		}
	}
}
