package pje.datamining;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import pje.parser.TrainingsetParser;

import org.json.simple.JSONObject;

public class NaiveBayes {
  private int N;
  private String[][] trainingset;

  private HashMap<String, Integer> positives;
  private HashMap<String, Integer> negatives;
  private HashMap<String, Integer> neutrals;
  private HashMap<String, Integer> all;

  public NaiveBayes(String trainingset) {
    TrainingsetParser parser = new TrainingsetParser(trainingset);
    this.trainingset = parser.getTrainingset();

    this.positives = new HashMap<String,Integer>();
    this.negatives = new HashMap<String,Integer>();
    this.neutrals = new HashMap<String,Integer>();
    this.all = new HashMap<String,Integer>();
  }


  public HashMap<String, Integer> getPositives() {
    return positives;
  }

  public void setPositives(HashMap<String, Integer> positives) {
    this.positives = positives;
  }

  public HashMap<String, Integer> getNegatives() {
    return negatives;
  }

  public void setNegatives(HashMap<String, Integer> negatives) {
    this.negatives = negatives;
  }

  public HashMap<String, Integer> getNeutrals() {
    return neutrals;
  }

  public void setNeutrals(HashMap<String, Integer> neutrals) {
    this.neutrals = neutrals;
  }


  public String[][] getTrainingset() {
    return trainingset;
  }

  public HashMap<String,HashMap<String,Integer>> OrganizeProba(boolean mode){
    System.out.println("begin organize proba");
    for (int i = 0; i < trainingset.length; i++) {
      if(trainingset[i][0] != null){
        String splitted[] = trainingset[i][0].split(" ");
        String mot;
        int j;
        if(mode) {
          j = 1;
        }
        else {
          j = 0;
        }
        for (; j < splitted.length; j++) {
          int classe = Integer.parseInt(trainingset[i][1]);
          if(mode) {
            mot = splitted[j-1] + " " + splitted[j];
          }
          else {
            mot = splitted[j];
          }

          all.put(mot,0);

          switch(classe){
            case (-1): if(mot.length() >= 3) putInHashMap(negatives, mot);break;
            case 0 : if(mot.length() >= 3) putInHashMap(neutrals, mot);break;
            case 1 : if(mot.length() >= 3) putInHashMap(positives, mot);break;
            default : break;

          }
        }

      }
    }


    HashMap<String,HashMap<String,Integer>> res = new HashMap<String,HashMap<String,Integer>>();
    res.put("negatives", negatives);
    res.put("neutrals", neutrals);
    res.put("positives", positives);
    res.put("all", all);

    System.out.println("end organize proba");

    return res;
  }

  //Fonction qui calcul la probabilité d'un mot sachant sa classe
  public double computeProba(HashMap<String,Integer> list,String[] tweet){
    int nbMots,nbMotsTotal;
    double proba;
    double probaMot[];
    double probaClasse;

    probaMot = new double[tweet.length];
    nbMots = list.size();
    nbMotsTotal = this.all.size();
    proba = 1;

    probaClasse = (double) nbMots / (double) (this.negatives.size()+this.positives.size() + this.neutrals.size());

    for (int i = 0; i < tweet.length; i++) {
      if(list.containsKey(tweet[i])){
        probaMot[i] = ((double) (list.get(tweet[i])+1) / (double) (nbMots + nbMotsTotal));
      }
      else {
        probaMot[i] = 1 / (double) (nbMots + nbMotsTotal);
      }

      proba *= (double)probaMot[i];
    }
    return (proba * (double)probaClasse);
  }

  public double computeProbaFrequence(HashMap<String,Integer> list,String[] tweet){
    int nbMots,nbMotsTotal;
    double proba;
    double probaMot[];
    double probaClasse;
    List<Integer> occurences;
    List<String> tweetAsList;
    boolean dejaVu[];

    probaMot = new double[tweet.length];
    nbMots = list.size();
    nbMotsTotal = this.all.size();
    proba = 1;
    occurences = new ArrayList<Integer>();
    tweetAsList = new ArrayList<String>();
    dejaVu = new boolean[tweet.length];

    probaClasse = (double) nbMots / (double) (this.negatives.size()+this.positives.size() + this.neutrals.size());


    for (int i = 0; i < tweet.length; i++) {
      tweetAsList.add(i,tweet[i]);
    }

    for (int i = 0; i < dejaVu.length; i++) {
      dejaVu[i] = false;
    }

    for (int i = 0; i < tweetAsList.size(); i++) {
      if(dejaVu[i] == false){
        dejaVu[i] = true;
        occurences.add(i,1);
        tweetAsList.remove(i);
      }else{
        occurences.add(i,occurences.get(i)+1);
        tweetAsList.remove(i);
      }
    }

    for (int i = 0; i < tweet.length; i++) {
      if(list.containsKey(tweet[i])){
        probaMot[i] = ((double) (list.get(tweet[i])+1) / (double) (nbMots + nbMotsTotal));
      }
      else {
        probaMot[i] = 1 / (double) (nbMots + nbMotsTotal);
      }
      if(dejaVu[i]){
        for (int j = 0; j < occurences.get(i); j++) {
          proba *= (double)probaMot[i];
        }
      }
      else {
        proba *= (double)probaMot[i];
      }
    }
    return (proba * (double)probaClasse);
  }
  //Fonction d'incrementation de compteur
  private void putInHashMap(HashMap<String,Integer> list,String mot){
    int val;
    if(list.containsKey(mot)){
      val = list.get(mot) + 1;
      list.put(mot,val);
    }else{
      list.put(mot, 1);
    }

  }

  //Fonctions qui compte les occurrences d'un mot
  private int countOccurences(String pattern,String mot[]){
    int cpt =0;
    for (int i = 0; i < mot.length; i++) {
      if(pattern == mot[i]){
        cpt++;
      }
    }
    return cpt;
  }

  public int predict(String tweet) {
    double pnegatif, pneutre, ppositif;
    String[] splitTweet;

    splitTweet = tweet.split(" ");

    pnegatif = computeProbaFrequence(getNegatives(), splitTweet);
    pneutre = computeProbaFrequence(getNeutrals(), splitTweet);
    ppositif = computeProbaFrequence(getPositives(), splitTweet);

    System.out.println("pnegatif: "+pnegatif);
    System.out.println("pneutre: "+pneutre);
    System.out.println("ppositif: "+ppositif);

    double max =  Math.max(pnegatif, Math.max(pneutre, ppositif));

    if(max == pnegatif) {
      return -1;
    }
    else if(max == pneutre) {
      return 0;
    }
    else if(max == ppositif) {
      return 1;
    }

    return 0;
  }

  public String[] predictBatch(String base) {
    System.out.println("predict batch");
    int nbpositives, nbnegatives, nbneutrals;
    double percpositives, percneutrals, percnegatives;
    TrainingsetParser parser = new TrainingsetParser(base);
    String[][] set = parser.getTrainingset();
    int currentPrediction;
    JSONObject datapositives, dataneutrals, datanegatives;
    String datas[];

    nbpositives = nbneutrals = nbnegatives = 0;


    int i = 0;
    while(set[i][0] != null) {
      currentPrediction = predict(set[i][0]);
      System.out.println(currentPrediction);

      if(currentPrediction == -1) {
        nbnegatives++;
      }
      else if(currentPrediction == 0) {
        nbneutrals++;
      }
      else {
        nbpositives++;
      }
      i++;
    }

    percpositives = (nbpositives * 100) / i+0.0;
    percneutrals = (nbneutrals * 100) / i+0.0;
    percnegatives = (nbnegatives * 100) / i+0.0;

    //percpositives = (nbpositives / 15+0.0) * 100;
    //percneutrals = (nbneutrals / 15+0.0) * 100;
    //percnegatives = (nbnegatives / 15+0.0) * 100;

    System.out.println("positives: "+percpositives);
    System.out.println("neutrals: "+percneutrals);
    System.out.println("negatives: "+percnegatives);

    datapositives = new JSONObject();
    datapositives.put("label", "positives");
    datapositives.put("data",percpositives);
    datapositives.put("color", "#4da74d");

    dataneutrals = new JSONObject();
    dataneutrals.put("label", "neutrals");
    dataneutrals.put("data",percneutrals);
    dataneutrals.put("color", "#afd8f8");

    datanegatives = new JSONObject();
    datanegatives.put("label", "negatives");
    datanegatives.put("data",percnegatives);
    datanegatives.put("color", "#cb4b4b");

    datas = new String[3];
    datas[0] = datanegatives.toString();
    datas[1] = dataneutrals.toString();
    datas[2] = datapositives.toString();

    return datas;
  }
}
