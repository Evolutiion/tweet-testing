package pje.datamining;

import pje.parser.TrainingsetParser;

import org.json.simple.JSONObject;

public class KNN {

  private int N;
  private String tweets[][];

  public KNN(String trainingset, int N) {
    TrainingsetParser parser = new TrainingsetParser(trainingset);
    tweets = parser.getTrainingset();
    this.N = N;
  }

  public double D(String tweet1, String tweet2) {
    int nbcommum = 0;
    int nbtotal = 0;
    String[] t1, t2;

    t1 = tweet1.split(" ");
    t2 = tweet2.split(" ");

    nbtotal = t1.length + t2.length;

    for(int i = 0; i < t1.length; i++) {
      for(int j = 0; j < t2.length; j++) {
        if(t1[i].equals(t2[j])) {
          nbcommum++;
        }
      }
    }

    return (nbtotal - nbcommum) / nbtotal;
  }

  public int predict(String tweet, int k) {
    int prochevoisin[] = new int[k];
    int pluseloigne;

    for(int i = 0; i < k; i++) {
      prochevoisin[i] = i;
    }

    for(int i = k+1; i < N; i++) {
      pluseloigne = 0;
      for(int j = 0; j < k; j++) {
        if(D(tweets[i][0], tweet) < D(tweets[prochevoisin[j]][0], tweet)) {
          if(D(tweets[pluseloigne][0], tweet) < D(tweets[prochevoisin[j]][0], tweet)) {
            pluseloigne = j;
          }
        }
      }
      prochevoisin[pluseloigne] = i;
    }
    return vote(prochevoisin, k);
  }

  public String[] predictBatch(String base, int k) {
    int nbpositives, nbnegatives, nbneutrals;
    int i;
    double percpositives, percneutrals, percnegatives;
    TrainingsetParser parser = new TrainingsetParser(base);
    String[][] set = parser.getTrainingset();
    int currentPrediction;
    JSONObject datapositives, dataneutrals, datanegatives;
    String datas[];

    nbpositives = nbneutrals = nbnegatives = 0;
    i = 0;

    while(set[i][0] != null) {
      currentPrediction = predict(set[i][0], k);

      if(currentPrediction == -1) {
        nbnegatives++;
      }
      else if(currentPrediction == 0) {
        nbneutrals++;
      }
      else {
        nbpositives++;
      }
      i++;
    }

    percpositives = (nbpositives * 100) / i+0.0;
    percneutrals = (nbneutrals * 100) / i+0.0;
    percnegatives = (nbnegatives * 100) / i+0.0;

    datapositives = new JSONObject();
    datapositives.put("label", "positives");
    datapositives.put("data",percpositives);
    datapositives.put("color", "#4da74d");

    dataneutrals = new JSONObject();
    dataneutrals.put("label", "neutrals");
    dataneutrals.put("data",percneutrals);
    dataneutrals.put("color", "#afd8f8");

    datanegatives = new JSONObject();
    datanegatives.put("label", "negatives");
    datanegatives.put("data",percnegatives);
    datanegatives.put("color", "#cb4b4b");

    datas = new String[3];
    datas[0] = datanegatives.toString();
    datas[1] = dataneutrals.toString();
    datas[2] = datapositives.toString();

    return datas;
  }

  public int vote(int[] prochevoisin, int k) {
    int nbpositif, nbnegatif, nbneutre;

    nbpositif = nbnegatif = nbneutre = 0;



    for(int i = 0; i < k; i++) {
      if(tweets[prochevoisin[i]][1].equals("-1")) {
        nbnegatif++;
      }
      if(tweets[prochevoisin[i]][1].equals("0")) {
        nbneutre++;
      }
      if(tweets[prochevoisin[i]][1].equals("1")) {
        nbpositif++;
      }
    }

    if(nbpositif <= nbneutre && nbneutre <= nbnegatif) {
      return -1;
    }
    else if(nbpositif <= nbneutre && nbneutre >= nbnegatif) {
      return 0;
    }
    else if(nbpositif >= nbneutre && nbpositif <= nbnegatif) {
      return -1;
    }
    else {
      return 1;
    }
  }
}
