package pje.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.Integer;
import java.io.IOException;

public class TrainingsetParser {

  private BufferedReader reader;
  private String trainingset[][];

  public TrainingsetParser(String csvfile) {
    try {
      File f = new File(csvfile);
      FileReader fr = new FileReader(f);
      reader = new BufferedReader(fr);
      int l = (int)f.length();
      trainingset = new String[l][2];
    }catch(IOException e) {
      System.out.println("Can't open file");
      e.printStackTrace();
    }
  }

  private void read() {
    int i = 0;
    String line;
    String splitline[];

    try {
      while((line = reader.readLine()) != null) {
        splitline = line.split(";");
        trainingset[i][0] = splitline[2];
        trainingset[i][1] = splitline[4];
        i++;
      }
    }catch(IOException e) {
      System.out.println("can't read file");
      e.printStackTrace();
    }
  }

  public String[][] getTrainingset() {
    read();
    return trainingset;
  }
}
