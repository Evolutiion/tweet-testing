package pje.parser;

import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;

public class CSVWriter {
  private File file;
  private char separator;
  private FileWriter writer;
  private BufferedWriter bf;

  public CSVWriter(String filepath) {
    this.file = new File(filepath);
    this.separator = ';';

    try {
      this.writer = new FileWriter(file, false);
      this.bf = new BufferedWriter(writer);
    }catch(IOException e) {
      System.out.println("IO Error");
      e.printStackTrace();
    }
  }

  public CSVWriter(String filepath, char separator) {
    this.file = new File(filepath);
    this.separator = separator;

    try {
      this.writer = new FileWriter(file, true);
      this.bf = new BufferedWriter(writer);
    }catch(IOException e) {
      System.out.println("IO Error");
      e.printStackTrace();
    }
  }

  public void writeLine(String[] elmts) {
    try {
      for(int i = 0; i < elmts.length; i++) {
        bf.write(elmts[i]+separator);
      }
      bf.write("\n");
      bf.flush();
    }catch(IOException e) {
      System.out.println("IO Error");
      e.printStackTrace();
    }
  }
}
